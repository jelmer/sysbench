Source: sysbench
Section: misc
Priority: optional
Maintainer: JCF Ploemen (jcfp) <linux@jcf.pm>
Build-Depends:
 debhelper-compat (= 13),
 default-libmysqlclient-dev,
 docbook-xml,
 docbook-xsl,
 libaio-dev [linux-any],
 libck-dev,
 libluajit-5.1-dev,
 libpq-dev,
 libssl-dev,
 pkg-config,
 python3-cram,
 txt2man,
 xsltproc
Standards-Version: 4.5.0
Homepage: https://github.com/akopytov/sysbench
Vcs-Git: https://salsa.debian.org/jcfp/sysbench.git
Vcs-Browser: https://salsa.debian.org/jcfp/sysbench

Package: sysbench
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: multi-threaded benchmark tool for database systems
 SysBench is a modular, scriptable and multi-threaded benchmark tool based on
 LuaJIT. It is most frequently used for database benchmarks, but can also be
 used to create arbitrarily complex workloads that do not involve a database
 server.
 .
 The idea of this benchmark suite is to quickly get an impression about system
 performance without setting up complex database benchmarks or even without
 installing a database at all.
 .
 Current features allow one to test the following system parameters:
 .
  * file I/O performance
  * scheduler performance
  * memory allocation and transfer speed
  * POSIX threads implementation performance
  * database server performance (OLTP benchmark)
 .
 Primarily written for MySQL server benchmarking, SysBench will be further
 extended to support multiple database backends, distributed benchmarks and
 third-party plug-in modules.
